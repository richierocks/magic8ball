#' Get magical advice
#'
#' Returns magic eightball advice.
#' @param n A positive integer of the number of pieces of advice to return.
#' @param weights A numeric vector of length 20 giving the weights for each
#' response, or \code{NULL} for equal weighting.
#' @return A character vector.
#' @references \url{http://service.mattel.com/us/productDetail.aspx?prodno=30188&siteid=27}
#' @examples
#' magic8ball()
#' magic8ball(10)
#' sort(table(magic8ball(1e4, weights = 1:20)))
#' @export
magic8ball <- function(n = 1, weights = NULL)
{
  answers <- c(
    "It is certain",
    "It is decidedly so",
    "Without a doubt",
    "Yes, definitely",
    "You may rely on it",
    "As I see it, yes",
    "Most likely",
    "Outlook good",
    "Yes",
    "Signs point to yes",
    "Reply hazy try again",
    "Ask again later",
    "Better not tell you now",
    "Cannot predict now",
    "Concentrate and ask again",
    "Don't count on it",
    "My reply is no",
    "My sources say no",
    "Outlook not so good",
    "Very doubtful"
  )
  sample(answers, n, replace = TRUE, prob = weights)
}
