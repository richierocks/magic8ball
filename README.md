[![Project Status: Active - The project has reached a stable, usable state and is being actively developed.](http://www.repostatus.org/badges/0.1.0/active.svg)](http://www.repostatus.org/#active)
[![Is the package on CRAN?](http://www.r-pkg.org/badges/version/magic8ball)](http://www.r-pkg.org/pkg/magic8ball)

# magic8ball

Simulate a [Magic 8 Ball](http://service.mattel.com/us/productDetail.aspx?prodno=30188&siteid=27).  Useful for [Magic 8 Ball Wednesday](http://27bslash6.com/magic8ball.html).

### Installation

To install the stable version, type:

```{r}
install.packages("magic8ball")
```

To install the development version, you first need the *devtools* package.

```{r}
install.packages("devtools")
```

Then you can install the *magic8ball* package using

```{r}
library(devtools)
install_bitbucket("richierocks/magic8ball")
```

### Functionality

`magic8ball()` returns one piece of fortune-telling advice.

`magic8ball(10, weights = 1:20)` returns 10 responses, with different weights for each of the 20 responses.
